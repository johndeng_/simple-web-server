# -*- coding: utf-8 -*-

import sys
sys.path.append('../')

import datetime
import socket
import StringIO
import sys


class WSGIServer(object):

    address_family = socket.AF_INET
    socket_type = socket.SOCK_STREAM
    request_queue_size = 1

    def __init__(self, server_address):

        self.listen_socket = listen_socket = socket.socket(
            self.address_family,
            self.socket_type
        )
        listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listen_socket.bind(server_address)
        listen_socket.listen(self.request_queue_size)
        host, port = self.listen_socket.getsockname()[:2]
        self.server_name = socket.getfqdn(host)
        self.server_port = port
        self.headers_set = []
        self.router = {}

    def serve_forever(self):
        listen_socket = self.listen_socket
        while True:
            self.client_connection, client_address = listen_socket.accept()
            self.handle_one_request()

    def handle_one_request(self):
        self.request_data = request_data = self.client_connection.recv(1024)
        self.parse_request(request_data)
        env = self.get_environ()

        try:

            app_name = None
            if 'django' in self.path:
                app_name = 'django'

            if 'tornado' in self.path:
                app_name = 'tornado'

            if 'flask' in self.path:
                app_name = 'flask'

            if 'bottle' in self.path:
                app_name = 'bottle'

            if not app_name:
                raise ValueError

            application = self.router[app_name]
            result = application(env, self.start_response)

        except:
            result = self.error_handler(env, self.start_response)

        self.finish_response(result)

    def parse_request(self, text):
        request_line = text.splitlines()[0]
        request_line = request_line.rstrip('\r\n')
        (self.request_method, self.path,
         self.request_version) = request_line.split()

    def get_environ(self):
        return {
            'wsgi.version': (1, 0),
            'wsgi.url_scheme': 'http',
            'wsgi.input': StringIO.StringIO(self.request_data),
            'wsgi.errors': sys.stderr,
            'wsgi.multithread': False,
            'wsgi.multiprocess': False,
            'wsgi.run_once': False,
            'REQUEST_METHOD': self.request_method,
            'PATH_INFO': self.path,
            'SERVER_NAME': self.server_name,
            'SERVER_PORT': str(self.server_port)
        }

    def start_response(self, status, response_headers):

        server_headers = [
            ('Date', datetime.datetime.now().strftime('%a, %d %b %Y %X %Z')),
            ('Server', 'Home make webserver'),
        ]
        self.headers_set = [status, response_headers + server_headers]

    def finish_response(self, result):
        try:
            status, response_headers = self.headers_set
            response = 'HTTP/1.1 {status}\r\n'.format(status=status)
            for header in response_headers:
                response += '{0}: {1}\r\n'.format(*header)
            response += '\r\n'
            for data in result:
                response += data
            self.client_connection.sendall(response)
        finally:
            self.client_connection.close()

    def error_handler(self, environ, start_response):
        status = '404 Not Found'
        response_headers = [('Content-Type', 'text/plain')]
        start_response(status, response_headers)
        return '404'


SERVER_ADDRESS = (HOST, PORT) = '', 8888


def make_server(server_address, router):
    server = WSGIServer(server_address)
    server.router = router
    return server


if __name__ == '__main__':

    from django_app import wsgi
    from tornado_app import application as tornado_application
    from flask_app import app as flask_application
    from bottle_app import application as bottle_application

    mini_router = {
        'django': wsgi.application,
        'tornado': tornado_application,
        'flask': flask_application,
        'bottle': bottle_application
    }

    httpd = make_server(SERVER_ADDRESS, mini_router)
    print('WSGIServer: Serving HTTP on port {port} ...\n'.format(port=PORT))
    httpd.serve_forever()
