# coding=utf-8

import tornado.web
import tornado.wsgi


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, Welcome to Tornado world")

app = tornado.web.Application([(r"/tornado", MainHandler)])
application = tornado.wsgi.WSGIAdapter(app)
