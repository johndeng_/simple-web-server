# coding:utf-8

import bottle
from bottle import route


@route('/bottle')
def hello():
    return "Hi, Welcome to Bottle World!"

application = bottle.default_app()
