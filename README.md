Simple Web Server
===

这是一个简单的 实现 WSGI 的 WebServer, 根据 URL 来进行分发到不同应用

运行
---

0. 安装依赖

```
pip install -r requirements.txt
```

1. 进入 `app` 目录启动 Server
```
$ cd app
$ python webserver.py
```

访问
---

#### Django

```
$ curl -l 127.0.0.1:8888/django
```

#### Tornado

```
$ curl -l 127.0.0.1:8888/tornado
```

#### Bottle

```
$ curl -l 127.0.0.1:8888/bottle
```

#### Flask

```
$ curl -l 127.0.0.1:8888/flask
```
